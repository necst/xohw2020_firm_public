Team number: xohw20_368

Project name: FPGA implementation of image registration metrics

Date: 20200630

Version of uploaded archive: 1

 

University name: Politecnico di Milano

Supervisor name: Marco D. Santambrogio

Supervisor e-mail: marco.santambrogio@polimi.it

Participant(s): Luigi Fusco, Davide Giacomini, Lorenzo Guerrieri

Email: luigifusco98@gmail.com, lorenzo.guerrieri1998@gmail.com, giacomini.davide@outlook.com

Board used: Ultra96

Software Version: Vitis 2019.2

Brief description of project: "FPGA implementation of image registration metrics" is an hardware implementation of three widely used image registration metrics, namely Mean Squared Error, Cross Correlation, and Mutual Information. The project targets Ultra96 borards.

Link to project repository: https://bitbucket.org/necst/xohw2020_firm_public

 

Description of archive (explain directory structure, documents and source files):

The `src` contains the source of the project. We developed three distinct kernels, one for each function. The code for each kernel is located in its dedicated subfolder.
Each subfolder contains three files:
- a host file, marked by `_host`
- a kernel file
- a header file called `host.hpp` containing utility code

The report is contained in the file `report.pdf`.
Compiled executable and bitstreams targeting linux are available at [this shared folder](https://polimi365-my.sharepoint.com/:f:/g/personal/10601210_polimi_it/Er3VLjGgzTdDidclJIsAvEQBnGnAr7LJY9vlTAlmj7KioA?e=tvzXFy), and are divided in one subfolder for each kernel.

Instructions to build and test project

All xclbin files can be obtained by compiling the kernel file in Vitis, selecting the U200 as the target platform, according to the normal workflow. The commands used to compile the host file on a linux machine with Vitis installed were
```
source /new_xilinx/software/Vitis/2019.2/settings64.sh
source /opt/xilinx/xrt/setup.sh
export XILINX_XRT=/opt/xilinx/xrt/
export XILINX_VITIS=/new_xilinx/software/Vitis/2019.2
g++ -I $XILINX_XRT/include/ -I $XILINX_VITIS/include/ --std=c++1y -L $XILINX_XRT/lib/ -lxilinxopencl -lpthread filename_host.cpp -o filename
```

The generated executable can be launched passing as an argument the name of the `xclbin` file and sourcing the runtime environment:
```
 source /opt/xilinx/xrt/setup.sh
 ./filename binfile.xclbin
```

 

Link to YouTube Video(s): https://youtu.be/d0duHRejpaU