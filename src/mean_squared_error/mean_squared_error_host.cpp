#include "host.hpp"

#define INPUT_SIZE 65536

void software_mean_squared_error(const unsigned char *If, const unsigned char *Im, float *result) {
    int partial = 0;
    for (int i = 0; i < INPUT_SIZE; ++i) partial += ((int)If[i] - (int)Im[i]) * ((int)If[i] - (int)Im[i]);

    *result = (float)partial / (float)INPUT_SIZE;
}

int main(int argc, char** argv) {
    cl_int err;

    std::vector<unsigned char,aligned_allocator<unsigned char>> source_If(INPUT_SIZE);
    std::vector<unsigned char,aligned_allocator<unsigned char>> source_Im(INPUT_SIZE);
    float fpga_result;

    // Create the test data 
    for(int i = 0 ; i < INPUT_SIZE ; i++){
        source_If[i] = rand() % 256;
        source_Im[i] = rand() % 256;
    }

// OPENCL HOST CODE AREA START

    std::vector<cl::Device> devices = get_devices();
    cl::Device device = devices[0];
    std::string device_name = device.getInfo<CL_DEVICE_NAME>();
    std::cout << "Found Device=" << device_name.c_str() << std::endl;

    //Creating Context and Command Queue for selected device
    cl::Context context(device);
    cl::CommandQueue q(context, device);

    // Import XCLBIN
    xclbin_file_name = argv[1];
    cl::Program::Binaries mean_squared_error_bins = import_binary_file();

    // Program and Kernel
    devices.resize(1);
    cl::Program program(context, devices, mean_squared_error_bins);
    cl::Kernel krnl_mean_squared_error(program, "mean_squared_error");

	
	// Allocate
    OCL_CHECK(err, cl::Buffer buffer_If   (context,CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, 
            INPUT_SIZE, source_If.data(), &err));

    OCL_CHECK(err, cl::Buffer buffer_Im   (context,CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, 
            INPUT_SIZE, source_Im.data(), &err));

    OCL_CHECK(err, cl::Buffer buffer_output(context,CL_MEM_USE_HOST_PTR | CL_MEM_WRITE_ONLY, 
            sizeof(float), &fpga_result, &err));

    OCL_CHECK(err, err = krnl_mean_squared_error.setArg(0, buffer_If));
    OCL_CHECK(err, err = krnl_mean_squared_error.setArg(1, buffer_Im));
    OCL_CHECK(err, err = krnl_mean_squared_error.setArg(2, buffer_output));


    // Copy input data to device global memory
    OCL_CHECK(err, err = q.enqueueMigrateMemObjects({buffer_If, buffer_Im},0/* 0 means from host*/));
    q.finish();

    auto start = std::chrono::high_resolution_clock::now();

    // Launch the Kernel
    OCL_CHECK(err, err = q.enqueueTask(krnl_mean_squared_error));
    q.finish();

    auto stop = std::chrono::high_resolution_clock::now();

    // Copy Result from Device Global Memory to Host Local Memory
    OCL_CHECK(err, err = q.enqueueMigrateMemObjects({buffer_output},CL_MIGRATE_MEM_OBJECT_HOST));
    q.finish();

    auto fpga_duration = std::chrono::duration_cast<std::chrono::nanoseconds>(stop - start);

    std::cout << "FPGA implementation took: " << fpga_duration.count() << " nanoseconds" << std::endl;

    start = std::chrono::high_resolution_clock::now();

    float cpu_result;
    software_mean_squared_error(source_If.data(), source_Im.data(), &cpu_result);

    stop = std::chrono::high_resolution_clock::now();

    auto cpu_duration = std::chrono::duration_cast<std::chrono::nanoseconds>(stop - start);

    std::cout << "CPU implementation took: " << cpu_duration.count() << " nanoseconds" << std::endl;

    std::cout << "Difference of: " << (float)cpu_duration.count() / (float)fpga_duration.count() << " times" << std::endl;

    std::cout << "FPGA result: " << fpga_result << std::endl << "CPU result: " << cpu_result << std::endl;
}