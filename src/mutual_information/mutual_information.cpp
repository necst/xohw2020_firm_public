#include "ap_int.h"
#include "hls_math.h"
#include "hls_video.h"

#define BINS 256
#define INPUT_SIZE 65536

#define KERNEL_SIZE 3
#define PADDING 1
#define COUNTERS_WIDTH (BINS+2*PADDING)
#define COUNTERS_SIZE (COUNTERS_WIDTH*COUNTERS_WIDTH)

// definition of fixed-precision and integer custom types
typedef ap_fixed<48, 24> mi_fixed;

// equally spaced b-spline of degree 4 (5 knots, of which 3 non zero)
const mi_fixed kernel[KERNEL_SIZE] = {0.167, 0.667, 0.167};

/* 	
	shifts coordinates to add padding
	used to index the estimators matrix using normal coordinates
	adds n = PADDING elements on top and to the left
*/
inline int get_padded_coordinates(int i, int j) {
	return (BINS + 2*PADDING)*(PADDING + i) + j + PADDING;
}

// top level function
// images as arrays of 8-bit intensity values
void mutual_information(const unsigned char* If, const unsigned char* Im, float *result) {
#pragma HLS INTERFACE m_axi port=If offset=slave bundle=gmem0
#pragma HLS INTERFACE m_axi port=Im offset=slave bundle=gmem1
#pragma HLS INTERFACE m_axi port=result offset=slave bundle=gmem2

#pragma HLS INTERFACE s_axilite port=If bundle=control
#pragma HLS INTERFACE s_axilite port=Im bundle=control
#pragma HLS INTERFACE s_axilite port=result bundle=control
#pragma HLS INTERFACE s_axilite port=return bundle=control

	mi_fixed
	// zero padded matrix counting the occurrences of pixel intensity pairs
		estimators[COUNTERS_SIZE],
	// cache for computing row convolution limiting the number of reads
		cache[KERNEL_SIZE],
	// partial estimators
		partial_i_estimators[BINS],
		partial_k_estimators[BINS];

#pragma HLS ARRAY_PARTITION variable=partial_i_estimators cyclic factor=4
#pragma HLS ARRAY_PARTITION variable=partial_k_estimators cyclic factor=4
#pragma HLS ARRAY_PARTITION variable=estimators cyclic factor=4
#pragma HLS ARRAY_PARTITION variable=cache complete


	// ---- INITIALIZATION ----
	initialize:for (int i = 0; i < COUNTERS_SIZE; ++i) {
#pragma HLS PIPELINE
#pragma HLS LOOP UNROLL factor=4
		estimators[i] = 0;
	}

	initialize_partial:for (int i = 0; i < BINS; ++i) {
#pragma HLS PIPELINE
#pragma HLS LOOP UNROLL factor=4
		partial_i_estimators[i] = 0;
		partial_k_estimators[i] = 0;
	}
	// ---- END INITIALIZATION ----


	// counts the number of occurrence of intensity pairs in the input images
	estimators:for(int i = 0; i < INPUT_SIZE; ++i) {
#pragma HLS PIPELINE
#pragma HLS LOOP UNROLL factor=4
		estimators[get_padded_coordinates(If[i], Im[i])]++;
	}

	// horizontal pass
	hconv:for (int row = PADDING; row < BINS+PADDING; ++row) {
	for (int col = 0; col < BINS+PADDING; ++col) {
#pragma HLS PIPELINE
			int pixel = row * COUNTERS_WIDTH + col;
			mi_fixed in_val = estimators[pixel];
			mi_fixed out_val = 0;
			for (int i = 0; i < KERNEL_SIZE; ++i) {
				cache[i] = i == KERNEL_SIZE - 1 ? in_val : cache[i+1];		// left shift of elements in cache, inserting new value from estimators
				out_val += cache[i] * kernel[i];							// performing actual convolution
			}
			if (col >= KERNEL_SIZE - 1) {
				estimators[row * COUNTERS_WIDTH + col - 1] = out_val;		// if position is valid, overwrites value in estimator which is already in cache
			}
		}
	}

	mi_fixed norm_factor = 0;

	// vertical pass
	vconv:for (int col = PADDING; col < BINS+PADDING; ++col) {
	for (int row = 0; row < BINS+PADDING; ++row) {
#pragma HLS PIPELINE
			int pixel = row * COUNTERS_WIDTH + col;
			mi_fixed in_val = estimators[pixel];
			mi_fixed out_val = 0;
			for (int i = 0; i < KERNEL_SIZE; ++i) {
				cache[i] = i == KERNEL_SIZE - 1 ? in_val : cache[i+1];
				out_val += cache[i] * kernel[i];
			}
			if (row >= KERNEL_SIZE - 1) {
				estimators[row * COUNTERS_WIDTH + col - 1] = out_val;
				norm_factor += out_val;
			}
		}
	}

	normalization:for(int i = PADDING; i < BINS+PADDING; ++i) {
		for(int j = PADDING; j < BINS+PADDING; ++j) {
#pragma HLS PIPELINE
#pragma HLS LOOP UNROLL factor=4
#pragma HLS dependence variable=estimators inter false
#pragma HLS dependence variable=norm_factor inter false
#pragma HLS dependence variable=norm_factor intra false
			int index = i * (COUNTERS_WIDTH) + j;
			estimators[index] = estimators[index] / norm_factor;
		}
	}

	partial_k:for(int i = 0; i < BINS; ++i) {
		for (int k = 0; k < BINS; ++k) {
#pragma HLS PIPELINE
#pragma HLS LOOP UNROLL factor=4
			partial_k_estimators[k] += estimators[get_padded_coordinates(i, k)];
		}
	}

	partial_i:for (int k = 0; k < BINS; ++k) {
		for(int i = 0; i < BINS; ++i) {
#pragma HLS PIPELINE
#pragma HLS LOOP UNROLL factor=4
			partial_i_estimators[i] += estimators[get_padded_coordinates(i, k)];
		}
	}

	mi_fixed sum = 0;

	partial_estimator_log:for(int i = 0; i < BINS; ++i) {
#pragma HLS PIPELINE
#pragma HLS LOOP UNROLL factor=4
		sum += hls::log2(partial_i_estimators[i]) + hls::log2(partial_k_estimators[i]);
	}

	sum *= - BINS;

	sum_calculation:for(int i = 0; i < BINS; ++i)
		for(int k = 0; k < BINS; ++k) {
#pragma HLS PIPELINE
			mi_fixed prob_ik = estimators[get_padded_coordinates(i, k)];
			if(prob_ik != 0) {
				sum += prob_ik * hls::log2(prob_ik);
			}
		}

	*result = (float) -sum;
}
