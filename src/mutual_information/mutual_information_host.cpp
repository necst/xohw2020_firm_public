#include "host.hpp"

#define BINS 256
#define INPUT_SIZE 65536

#define KERNEL_SIZE 3
#define PADDING 1
#define COUNTERS_WIDTH (BINS+2*PADDING)
#define COUNTERS_SIZE (COUNTERS_WIDTH*COUNTERS_WIDTH)

// equally spaced b-spline of degree 4 (5 knots, of which 3 non zero)
const float kernel[KERNEL_SIZE] = {0.167, 0.667, 0.167};

/* 	
	shifts coordinates to add padding
	used to index the estimators matrix using normal coordinates
	adds n = PADDING elements on top and to the left
*/
int get_padded_coordinates(int i, int j) {
	return (BINS + 2*PADDING)*(PADDING + i) + j + PADDING;
}

void software_mutual_information(const unsigned char *If, const unsigned char *Im, float *result) {
    float
	// zero padded matrix counting the occurrences of pixel intensity pairs
		estimators[COUNTERS_SIZE],
	// cache for computing row convolution limiting the number of reads
		cache[KERNEL_SIZE],
	// partial estimators
		partial_i_estimators[BINS],
		partial_k_estimators[BINS];

	// ---- INITIALIZATION ----
	initialize:for (int i = 0; i < COUNTERS_SIZE; ++i) {
		estimators[i] = 0;
	}

	initialize_partial:for (int i = 0; i < BINS; ++i) {
		partial_i_estimators[i] = 0;
		partial_k_estimators[i] = 0;
	}
	// ---- END INITIALIZATION ----


	// counts the number of occurrence of intensity pairs in the input images
	estimators:for(int i = 0; i < INPUT_SIZE; ++i) {
		estimators[get_padded_coordinates(If[i], Im[i])]++;
	}

	// horizontal pass
	hconv:for (int row = PADDING; row < BINS+PADDING; ++row) {
	for (int col = 0; col < BINS+PADDING; ++col) {
			int pixel = row * COUNTERS_WIDTH + col;
			float in_val = estimators[pixel];
			float out_val = 0;
			for (int i = 0; i < KERNEL_SIZE; ++i) {
				cache[i] = i == KERNEL_SIZE - 1 ? in_val : cache[i+1];		// left shift of elements in cache, inserting new value from estimators
				out_val += cache[i] * kernel[i];							// performing actual convolution
			}
			if (col >= KERNEL_SIZE - 1) {
				estimators[row * COUNTERS_WIDTH + col - 1] = out_val;		// if position is valid, overwrites value in estimator which is already in cache
			}
		}
	}

	float norm_factor = 0;

	// vertical pass
	vconv:for (int col = PADDING; col < BINS+PADDING; ++col) {
	for (int row = 0; row < BINS+PADDING; ++row) {
			int pixel = row * COUNTERS_WIDTH + col;
			float in_val = estimators[pixel];
			float out_val = 0;
			for (int i = 0; i < KERNEL_SIZE; ++i) {
				cache[i] = i == KERNEL_SIZE - 1 ? in_val : cache[i+1];
				out_val += cache[i] * kernel[i];
			}
			if (row >= KERNEL_SIZE - 1) {
				estimators[row * COUNTERS_WIDTH + col - 1] = out_val;
				norm_factor += out_val;
			}
		}
	}

	normalization:for(int i = PADDING; i < BINS+PADDING; ++i) {
		for(int j = PADDING; j < BINS+PADDING; ++j) {
			int index = i * (COUNTERS_WIDTH) + j;
			estimators[index] = estimators[index] / norm_factor;
		}
	}

	partial_k:for(int i = 0; i < BINS; ++i) {
		for (int k = 0; k < BINS; ++k) {
			partial_k_estimators[k] += estimators[get_padded_coordinates(i, k)];
		}
	}

	partial_i:for (int k = 0; k < BINS; ++k) {
		for(int i = 0; i < BINS; ++i) {
			partial_i_estimators[i] += estimators[get_padded_coordinates(i, k)];
		}
	}

	float sum = 0;

	partial_estimator_log:for(int i = 0; i < BINS; ++i) {
		sum += std::log2(partial_i_estimators[i]) + std::log2(partial_k_estimators[i]);
	}

	sum *= - BINS;

	sum_calculation:for(int i = 0; i < BINS; ++i)
		for(int k = 0; k < BINS; ++k) {
			float prob_ik = estimators[get_padded_coordinates(i, k)];
			if(prob_ik != 0) {
				sum += prob_ik * std::log2(prob_ik);
			}
		}

	*result = (float) -sum;
}

int main(int argc, char** argv) {
    cl_int err;

    std::vector<unsigned char,aligned_allocator<unsigned char>> source_If(INPUT_SIZE);
    std::vector<unsigned char,aligned_allocator<unsigned char>> source_Im(INPUT_SIZE);
    float fpga_result;

    // Create the test data 
    for(int i = 0 ; i < INPUT_SIZE ; i++){
        source_If[i] = rand() % 256;
        source_Im[i] = rand() % 256;
    }

// OPENCL HOST CODE AREA START

    std::vector<cl::Device> devices = get_devices();
    cl::Device device = devices[0];
    std::string device_name = device.getInfo<CL_DEVICE_NAME>();
    std::cout << "Found Device=" << device_name.c_str() << std::endl;

    //Creating Context and Command Queue for selected device
    cl::Context context(device);
    cl::CommandQueue q(context, device);

    // Import XCLBIN
    xclbin_file_name = argv[1];
    cl::Program::Binaries mutual_information_bins = import_binary_file();

    // Program and Kernel
    devices.resize(1);
    cl::Program program(context, devices, mutual_information_bins);
    cl::Kernel krnl_mutual_information(program, "mutual_information");

	
	// Allocate
    OCL_CHECK(err, cl::Buffer buffer_If   (context,CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, 
            INPUT_SIZE, source_If.data(), &err));

    OCL_CHECK(err, cl::Buffer buffer_Im   (context,CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY, 
            INPUT_SIZE, source_Im.data(), &err));

    OCL_CHECK(err, cl::Buffer buffer_output(context,CL_MEM_USE_HOST_PTR | CL_MEM_WRITE_ONLY, 
            sizeof(float), &fpga_result, &err));

    OCL_CHECK(err, err = krnl_mutual_information.setArg(0, buffer_If));
    OCL_CHECK(err, err = krnl_mutual_information.setArg(1, buffer_Im));
    OCL_CHECK(err, err = krnl_mutual_information.setArg(2, buffer_output));


    // Copy input data to device global memory
    OCL_CHECK(err, err = q.enqueueMigrateMemObjects({buffer_If, buffer_Im},0/* 0 means from host*/));
    q.finish();

    auto start = std::chrono::high_resolution_clock::now();

    // Launch the Kernel
    OCL_CHECK(err, err = q.enqueueTask(krnl_mutual_information));
    q.finish();

    auto stop = std::chrono::high_resolution_clock::now();

    // Copy Result from Device Global Memory to Host Local Memory
    OCL_CHECK(err, err = q.enqueueMigrateMemObjects({buffer_output},CL_MIGRATE_MEM_OBJECT_HOST));
    q.finish();

    auto fpga_duration = std::chrono::duration_cast<std::chrono::nanoseconds>(stop - start);

    std::cout << "FPGA implementation took: " << fpga_duration.count() << " nanoseconds" << std::endl;

    start = std::chrono::high_resolution_clock::now();

    float cpu_result;
    software_mutual_information(source_If.data(), source_Im.data(), &cpu_result);

    stop = std::chrono::high_resolution_clock::now();

    auto cpu_duration = std::chrono::duration_cast<std::chrono::nanoseconds>(stop - start);

    std::cout << "CPU implementation took: " << cpu_duration.count() << " nanoseconds" << std::endl;

    std::cout << "Difference of: " << (float)cpu_duration.count() / (float)fpga_duration.count() << " times" << std::endl;

    std::cout << "FPGA result: " << fpga_result << std::endl << "CPU result: " << cpu_result << std::endl;
}