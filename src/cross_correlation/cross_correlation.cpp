#include "string.h"
#include "ap_int.h"

#define DIM 65536
#define WIDTH 512
#define CHAR_BITS (sizeof(unsigned char) * 8)
#define CHAR_PER_ELEM (WIDTH/CHAR_BITS)


inline unsigned char get_char(ap_int<WIDTH> ap, int index)
{
	return ap.range((index+1)*CHAR_BITS - 1, index*CHAR_BITS);
}

void cross_correlation(ap_int<WIDTH>* If, ap_int<WIDTH>* Im, float *result){
	#pragma HLS INTERFACE m_axi port=If offset=slave bundle=gmem0
	#pragma HLS INTERFACE m_axi port=Im offset=slave bundle=gmem1
	#pragma HLS INTERFACE m_axi port=result offset=slave bundle=gmem2

	#pragma HLS INTERFACE s_axilite port=If bundle=control
	#pragma HLS INTERFACE s_axilite port=Im bundle=control
	#pragma HLS INTERFACE s_axilite port=result bundle=control
	#pragma HLS INTERFACE s_axilite port=return bundle=control

	unsigned int sum;
	int i, j;

	sum_loop: for(i=0; i<DIM/CHAR_PER_ELEM; i++){
		#pragma HLS PIPELINE

		inner_sum_loop: for (j=0; j<CHAR_PER_ELEM; j++){
			#pragma HLS PIPELINE
			#pragma HLS UNROLL factor = 32
			sum += ((unsigned int) get_char(If[i], j)) * ((unsigned int)get_char(Im[i], j));
		}
	}

	*result = (float)sum/(float)DIM;
}
