# FIRM: FPGA implementation of Image Registration Metrics
This is the official report of the project FIRM submitted to the 2020 Xilinx Open Hardware competition.

The video presentation of the project is available [here](https://youtu.be/d0duHRejpaU).

Compiled executable and bitstreams targeting linux are available [here](https://polimi365-my.sharepoint.com/:f:/g/personal/10601210_polimi_it/Er3VLjGgzTdDidclJIsAvEQBnGnAr7LJY9vlTAlmj7KioA?e=tvzXFy).

The official repo of the project is available [here](https://bitbucket.org/necst/xohw2020_firm_public).

FIRM is an hardware implementation of three widely used image registration metrics, namely Mean Squared Error, Cross Correlation, and Mutual Information. The project targets Ultra96 borards, but the instructions in this file regard the compilation and testing on Xilinx' Alveo U200 Data Center Accelerator Card.

The project was developed at Politecnico di Milano with the support of NECSTLab.

## Project Structure

The `src` contains the source of the project. We developed three distinct kernels, one for each function. The code for each kernel is located in its dedicated subfolder.
Each subfolder contains three files:
- a host file, marked by `_host`
- a kernel file
- a header file called `host.hpp` containing utility code

The report is contained in the file `report.pdf`.
Compiled executable and bitstreams targeting linux are available at [this shared folder](https://polimi365-my.sharepoint.com/:f:/g/personal/10601210_polimi_it/Er3VLjGgzTdDidclJIsAvEQBnGnAr7LJY9vlTAlmj7KioA?e=tvzXFy), and are divided in one subfolder for each kernel.

## Usage

All xclbin files can be obtained by compiling the kernel file in Vitis, selecting the U200 as the target platform, according to the normal workflow. The commands used to compile the host file on a linux machine with Vitis installed were
```bash
source /new_xilinx/software/Vitis/2019.2/settings64.sh
source /opt/xilinx/xrt/setup.sh
export XILINX_XRT=/opt/xilinx/xrt/
export XILINX_VITIS=/new_xilinx/software/Vitis/2019.2
g++ -I $XILINX_XRT/include/ -I $XILINX_VITIS/include/ --std=c++1y -L $XILINX_XRT/lib/ -lxilinxopencl -lpthread filename_host.cpp -o filename
```

The generated executable can be launched passing as an argument the name of the `xclbin` file and sourcing the runtime environment:
```bash
 source /opt/xilinx/xrt/setup.sh
 ./filename binfile.xclbin
```

## Special Thanks
We would like to thank Davide Conficconi, Emanuele Del Sozzo and Marco D. Santambrogio for their support with the project.

### Full Links
video presentation: https://youtu.be/d0duHRejpaU

executables and bitstreams: https://polimi365-my.sharepoint.com/:f:/g/personal/10601210_polimi_it/Er3VLjGgzTdDidclJIsAvEQBnGnAr7LJY9vlTAlmj7KioA?e=tvzXFy

official repo: https://bitbucket.org/necst/xohw2020_firm_public